const bookmarkList = [
	{
		header: 'Header-1', bookmarks: [
			{ title: 'Bookmark-1', link: 'http://www.google.com'},
			{ title: 'Bookmark-2', link: 'http://www.google.com'},
			{ title: 'Bookmark-3', link: 'http://www.google.com'},
			{ title: 'Bookmark-4', link: 'http://www.google.com'},
			{ title: 'Bookmark-5', link: 'http://www.google.com'}
		]
	},
	{
		header: 'Header-2', bookmarks: [
			{ title: 'Bookmark-1', link: 'http://www.google.com'},
			{ title: 'Bookmark-2', link: 'http://www.google.com'},
			{ title: 'Bookmark-3', link: 'http://www.google.com'},
			{ title: 'Bookmark-4', link: 'http://www.google.com'}
		]
	},
	{
		header: 'Header-3', bookmarks: [
			{ title: 'Bookmark-1', link: 'http://www.google.com'},
			{ title: 'Bookmark-2', link: 'http://www.google.com'},
			{ title: 'Bookmark-3', link: 'http://www.google.com'}
		]
	},
	{
		header: 'Header-4', bookmarks: [
			{ title: 'Bookmark-1', link: 'http://www.google.com'},
			{ title: 'Bookmark-2', link: 'http://www.google.com'}
		]
	}
]

// The same as "onload"
window.addEventListener('load', (event) => {
    let today = new Date();
    let time = today.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', second: '2-digit' });
    let weekday = today.toLocaleString([], { weekday: 'long'});
    let date = today.toLocaleString([], { month: 'long', day: 'numeric', year: 'numeric'});
    displayTime(time, weekday, date);
    createBookmarkTable();
});

setInterval(function () {
    var today = new Date();
    var time = today.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', second: "2-digit" });
    let weekday = today.toLocaleString([], { weekday: 'long'});
    let date = today.toLocaleString([], { month: 'long', day: 'numeric', year: 'numeric'});
    document.getElementById("time").innerHTML= time;
    document.getElementById("weekday").innerHTML= weekday;
    document.getElementById("date").innerHTML= date;
}, 1000);

function displayTime(time, weekday, date) {
    document.getElementById("time").innerHTML = time;
    document.getElementById("weekday").innerHTML = weekday;
    document.getElementById("date").innerHTML = date;
}

function createBookmarkTable() {
	const table = document.createElement("table");
	table.id = "bookmarks";
	for (let i = 0; i < bookmarkList.length; i++) {
		const row = bookmarkList[i];
		const header = row.header;
		let tr = table.insertRow(-1);
		let td = document.createElement("td");

		td.innerHTML = `<h3>${header}</h3>`;
		td.className = 'header-element';
		tr.appendChild(td);

		for (let j in row.bookmarks) {
			const title = row.bookmarks[j].title;
			const link = row.bookmarks[j].link;
			let urlTag = document.createElement("a");
			urlTag.setAttribute('href', link);
			urlTag.innerHTML = title;

			let cell = tr.insertCell(-1);
			cell.appendChild(urlTag);
		}
	}
	const mainContainer = document.getElementById('main-container');
	mainContainer.appendChild(table);
}
